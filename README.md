# GPSbroker

Fetch shared NMEA data provided by mobile App such as [Share GPS](http://sharegps.jillybunch.com) and publish over a [ZeroMQ](http://zeromq.org/) socket in Json format.

# Installation

Clone the source tree:
```sh
git clone https://gitlab.com/gofleetsolutions/gpsbroker.git
```

Install gpsbroker with:
```sh
python3 setup.py install
```

# Usage
From the command line, connect to Share GPS:
```sh
gpsbroker --host <ip-address> --port <port> --addr 'ipc:///tmp/gps'
```

Or publishing over tcp (port 55555)
```sh
gpsbroker --host <ip-address> --port <port>  --addr 'tcp://*:55555'
```

Publish with a custom topic:
```sh
gpsbroker --host <ip-address> --port <port> --addr 'ipc:///tmp/gps' --topic GPSDATA
```

Adjust the delay between two consecutive fetch:
```sh
gpsbroker --host <ip-address> --port <port> --addr 'ipc:///tmp/gps' --delay 2
```

# Output format

Results are then published over a ZeroMQ socket in Json format:
```json
{
"timestamp": "1900-01-01T13:42:36Z",
"latitude": 48.858093383333336,
"longitude": 2.294694933333333
}
```

# Reports

* [Pylint report](https://gofleetsolutions.gitlab.io/gpsbroker/quality-report/pylint-report.html)
* [Coverage report](https://gofleetsolutions.gitlab.io/gpsbroker/coverage-report/index.html)
* [Unit tests report](https://gofleetsolutions.gitlab.io/gpsbroker/test-report/test_report.html)
