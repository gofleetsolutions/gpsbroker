# -*- coding: utf-8 -*-
# Copyright 2018-2019 GO FLEET SOLUTIONS S.R.L. See LICENSE for details.
"""
gpsbroker entry point
"""

import argparse
import datetime
import json
import os
import socket
import sys
import time

import attr
import zmq

import pynmea2

from . import __version__

VERSION_INTRO = r'scanner v{}'.format(__version__)

PACKET_SIZE = 1024


def determine_path():
    try:
        root = __file__
        if os.path.islink(root):
            root = os.path.realpath(root)
        return os.path.dirname(os.path.abspath(root))

    except:
        print("Invalid __file__ variable.")
        sys.exit()


def parse_args():
    parser = argparse.ArgumentParser(description='Collect NMEA GPS data and publish to 0MQ sockets.')
    parser.add_argument('-u', '--host', dest='gps_host', required=True,
                        help='GPS host')
    parser.add_argument('-p', '--port', dest='gps_port', type=int, default=50000,
                        help='GPS port (default=50000).')
    parser.add_argument('-d', '--delay', dest='delay', type=int, default=1,
                        help='Delay in seconds between two fetch (default=1).')
    parser.add_argument('-a', '--addr', dest='address', action='append', type=str,
                        help='0MQ socket address to publish data, in the form ‘protocol://interface:port’. '
                        'You may set this option multiple times.')
    parser.add_argument('-t', '--topic', dest='topic', default='GPS',
                        help='OMQ data topic (default=\'GPS\').')
    parser.add_argument('-q', '--quiet', dest='quiet', action='store_true',
                        help='Disable logging of scan results.')
    parser.add_argument('-V', '--version', dest='show_version', action='store_true')

    return vars(parser.parse_args())


class Publisher:
    def __init__(self, addrs, topic, encoding='utf-8'):
        self._context = zmq.Context()
        self._publisher = self._context.socket(zmq.PUB)
        self._topic = topic
        self._encoding = encoding

        if isinstance(addrs, str):
            addrs = (addrs,)

        for addr in addrs:
            self._publisher.bind(addr)

    def send(self, data):
        multipart_msg = [part.encode(self._encoding) for part in [self._topic, data]]
        self._publisher.send_multipart(multipart_msg)


@attr.s
class GpsData:
    datetime = attr.ib()
    timestamp = attr.ib()
    latitude = attr.ib()
    longitude = attr.ib()


def parse_nmea_data(nmea_data):
    if nmea_data:
        for line in nmea_data.decode('utf-8').splitlines():
            try:
                msg = pynmea2.parse(line)
                if hasattr(msg, 'latitude') and \
                   hasattr(msg, 'longitude') and \
                   hasattr(msg, 'timestamp'):
                    return GpsData(datetime=datetime.datetime.now().strftime('%Y-%m-%dT%H:%M:%SZ'),
                                   timestamp=msg.timestamp.strftime('%Y-%m-%dT%H:%M:%SZ'),
                                   latitude=msg.latitude,
                                   longitude=msg.longitude)
            except:
                pass

    return None


def main():
    arguments = parse_args()
    if arguments.get('show_version'):
        print(VERSION_INTRO)
        return

    topic = arguments.get('topic')
    addrs = arguments.get('address')
    publisher = None
    if addrs:
        publisher = Publisher(addrs, topic)

    host = arguments.get('gps_host')
    port = arguments.get('gps_port')

    delay = arguments.get('delay')

    while True:
        try:
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            sock.connect((host, port))
            break

        except OSError as err:
            print(err)

    while True:
        try:
            start = time.time()

            data = sock.recv(PACKET_SIZE)
            gps_data = parse_nmea_data(data)

            if gps_data:
                if publisher:
                    publisher.send(json.dumps(attr.asdict(gps_data)))

                if not arguments.get('quiet'):
                    print(gps_data)

            try:
                elapsed = time.time() - start
                time.sleep(delay - elapsed)

            except ValueError:
                pass

        except KeyboardInterrupt:
            break


if __name__ == '__main__':
    main()
