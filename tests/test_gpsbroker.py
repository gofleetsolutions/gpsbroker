# -*- coding: utf-8 -*-
# Copyright 2018-2019 GO FLEET SOLUTIONS S.R.L. See LICENSE for details.
"""
@package test_gpsbroker

This package implements the gpsbroker unit tests
"""

import sys
import unittest
from unittest.mock import patch

from gpsbroker.main import parse_args

DEFAULT_ARGUMENTS = {'gps_host': 'HOST_NAME',
                     'gps_port': 50000,
                     'delay': 1,
                     'address': None,
                     'topic': 'GPS',
                     'verbose': False,
                     'show_version': False}


class TCGpsbroker(unittest.TestCase):
    """ Implements unit tests for the gpsbroker
    """
    def test_parse_args(self):
        """ Check the arguments parser
        """
        testargs = ["gpsbroker", "--host", "HOST_NAME"]
        with patch.object(sys, 'argv', testargs):
            arguments = parse_args()
            print(arguments)
            self.assertEqual(arguments, DEFAULT_ARGUMENTS)
